<!DOCTYPE HTML>
<!--
	Horizons by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<?php include 'message/config.php'; ?>
<html>
	<head>
		<title>Happy Birthday Namita!</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
	    <script src="js/jquery.emojiarea.js"></script>
	    <script src="packs/basic/emojis.js"></script>
		<noscript>
			<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
		</noscript>

        <!-- FB --> 
        <meta property="og:title" content="Happy Birthday Namita!" /> 
        <meta property="og:description" content="A suprise birthday website" /> 
        <meta property="og:type" content="website" />
        <meta property="og:url" content="http://namitanisal.com" />
        <meta property="og:image" content="http://namitanisal.com/front/images/Namita.jpg" />
        <meta property="og:site_name" content="appy Birthday Namita!" />
        <meta property="fb:admins" content="" />

		<style type="text/css">
            body {
                overflow: hidden;
            }

            #preloader {
                position: fixed;
                top:0;
                left:0;
                right:0;
                bottom:0;
                background-color:#000; /* change if the mask should have another color then white */
                z-index:99; /* makes sure it stays on top */
                font-size: 16px;
            }

            #status {
                width:200px;
                height:200px;
                position:absolute;
                left:50%; /* centers the loading animation horizontally one the screen */
                top:50%; /* centers the loading animation vertically one the screen */
                background-image:url(img/status.gif); /* path to your loading animation */
                background-repeat:no-repeat;
                background-position:center;
                margin:-100px 0 0 -100px; /* is width and height divided by two */
                text-align: center;
            }


			textarea, .emoji-wysiwyg-editor {
				width: 100%;
				min-height: 100px;
				resize: none;
				padding:15px 15px 15px 15px;
				border-radius:5px;
				box-shadow:inset 4px 6px 10px -4px rgba(0,0,0,0.3), 0 1px 1px -1px rgba(255,255,255,0.3);
				background:rgba(206, 204, 204, 0.2);
				outline:none;
				border:none;
				border:1px solid rgba(0,0,0,1);
				margin-bottom:30px;
				margin-right:10px;
				color: #D1C5C5;
				text-shadow:#000 0px 1px 5px;
				font-size: 18px;
                text-align: justify;
			}
            .emoji-wysiwyg-editor img {
                width: 25px;
                height: 25px;
                vertical-align: middle;
                margin: -3px 0 0 0;
            }
            a.btn {
                display: inline-block;
                color: #666;
                background-color: #000;
                text-transform: uppercase;
                letter-spacing: 1px;
                font-size: 15px;
                padding: 10px 30px;
                border-radius: 5px;
                -moz-border-radius: 5px;
                -webkit-border-radius: 5px;
                border: 1px solid rgba(0,0,0,0.3);
                border-bottom-width: 3px;
                font-weight: 500;
                text-decoration: none;
            }

                a.btn:hover {
                    background-color: #666;
                    border-color: rgba(0,0,0,0.5);
                }
                
                a.btn:active {
                    background-color: #CCC;
                    border-color: rgba(0,0,0,0.9);
                }
		</style>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body class="homepage">

        <!-- Loading Section -->
        <div id="preloader">
            <div id="status"><br></div>
        </div>

		<!-- Header -->
			<div id="header">
				<div class="container">

					<!-- Banner -->
						<div id="banner">
							<div class="container">
								<section>
									<header class="major">
										<h2>Happy Birthday Namita!</h2>
										<!-- <span class="byline">&hellip; a responsive HTML5 site template freebie by <a href="http://templated.co">TEMPLATED</a>. Released for free under the <a href="http://templated.co/license">Creative Commons Attribution</a> license, so use it for whatever (personal or commercial) &ndash; just give us credit!</span> -->
									</header>
								</section>			
							</div>
						</div>

				</div>
			</div>

		<!-- Featured -->
			<div class="wrapper style2">
				<section class="container">
					<header class="major">
						<!-- <h2>Nulla luctus eleifend</h2> -->
						<span class="byline" style="line-height: 33px;">
                            Happy birthday to the most talented person we know,<br>
                            Nothing we can say that can possibly show,<br>
                            That it's truly great to have you around,<br>
                            So amazing you are; yet still on ground.<br>

<br>                        Within you, we've found the perfect friend,<br>
                            Someone who we know, will be there till the end.<br>
                            It's about 4 years, that on friendships path we walked,<br>
                            With you beside us, our lifes have truly rocked.<br>
                            We can be quite silly with you,<br>
                            Believe it, you are one of the very few.<br>

<br>                        We were glad, that our birthdays came,<br>
                            For you made for us cards which were never same.<br>
                            And BAM! Surprise came down in the form of portraits,<br>
                            You would've got straight A++, if there were any grades.<br>

<br>                        Our friendship with you grew each year,<br>
                            We are still together, we are still here.<br>
                            And a special bond that we hold,<br>
                            It's more precious than worlds all gold.<br>

<br>                        Never seen a girl like you,<br>
                            Anywhere you go, success you brew.<br>
                            You are the best in what you do,<br>
                            One of its own kind; yes that's true.<br>

<br>                        You know how to keep our lifes in line,<br>
                            You are that one person, who makes everything fine.<br>
                            And when life leaves us with no choice,<br>
                            All we do is listen to your voice.<br>
                            For you are there to help in any possible way,<br>
                            And the best part? We never have to ask; we never have to say.<br>

<br>                        You've stood by our side, like a strong pillar,<br>
                            Jokes you've been warned! She is a serial killer!<br>
                            Right from your <i>choodah</i> love to the chocolate craze,<br>
                            With something new, you never fail to amaze.<br>

<br>                        A lot has been said; yet a lot remains to say,<br>
                            This was simply our way to wish you...<br>
                            A VERY HAPPY BIRTHDAY!<br>
                            We wish that all your dreams come true,<br>
                            Because <i>tujhse he hoti hai humari khushiya shuru!</i><br>
                        </span>
					</header>
				</section>
			</div>

		<!-- Footer -->
			<div id="footer">
				<div class="container">
                    <h1 style="text-align:center;font-size: 42px;">Birthday Wishes<br>
                    <span><a class="btn" href="http://namitanisal.com/message" style="padding: 0px 15px;">Add a wish!</a></span></h1>
					<!-- Lists -->
						<div class="row">
							<div class="5u">
								<section>
									<div class="row">
										<section class="12u left">
										</section>
									</div>
								</section>
							</div>
							<div class="2u"><br></div>
							<div class="5u">
								<section>
									<section class="12u right">
									</section>
								</section>
							</div>
						</div>
                        <div style="text-align:center;">
                            <a class="btn" onclick="ajaxMessage()">More ...</a>
                        </div>

				</div>
			</div>
			<script type="text/javascript">
		        var random = Math.round(Math.random()*100);
                var noOfWishes = 0;
                var leftLength = 0;
                var rightLength = 0;

                ajaxMessage()

                function ajaxMessage()
                {
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "messageinfo.php",
                        data: {'off': noOfWishes,'random':random},
                        success: function(data) {
                            data = JSON.parse(data);
                            if(data.length != 0)
                            {
                                for(var i=0;i<data.length;i++){

                                    leftLength = $('.12u.left').position().top + $('.12u.left').outerHeight();
                                    rightLength = $('.12u.right').position().top + $('.12u.right').outerHeight();

                                    if(leftLength <= rightLength)
                                    {
                                        addMessage("left",data[i]);
                                    }
                                    else
                                    {
                                        addMessage("right",data[i]);
                                    }
                                    noOfWishes++;
                                    var container = ".emojis-wysiwyg." + data[i].facebookid;
                                    var $wysiwyg = $(container).emojiarea({wysiwyg: true});
                                    $('.emoji-wysiwyg-editor').attr('contenteditable', "false");
                                    $(".emoji-button").hide();

                                }
                            }
                            if (data.length < 10)
                            {
                                $(".btn").hide();
                            }
                        }
                    });
                }

                function addMessage(position, mObject)
                {
                    var container = ".12u." + position;
                    $(container).append("<textarea class=\"emojis-wysiwyg " + mObject.facebookid + "\">" + mObject.quote.trim() + "<br><br><div class=\"text-right\" style=\"text-align:right\"><img style=\"height:40px; width:40px;\" src=\"http://graph.facebook.com/" + mObject.facebookid + "/picture\" />  <span>" + mObject.name + "</span>&nbsp&nbsp</div>" + "</textarea>");
                }
		      </script>
              <script type="text/javascript">
                        var tid = setInterval( function () {
                            if ( document.readyState !== 'complete' ) return;
                            clearInterval( tid );       
                            $('#status').fadeOut(); // will first fade out the loading animation
                            $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                            $('body').delay(350).css({'overflow':'visible'});
                        }, 5 );
                </script> 
	</body>
</html>