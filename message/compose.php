<?php 
include 'config.php';

//Login or logout url will be needed depending on current user state.
if ($user) {
  $logoutUrl = $facebook->getLogoutUrl();
  
} else {
  $loginUrl = $facebook->getLoginUrl(array('scope' =>''));
  header('Location:'.$loginUrl);
}
?>
    
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>Birthday Wishes For Namita</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    
    <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href="css/signin.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery.emojiarea.css">

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.emojiarea.js"></script>
    <script src="../packs/basic/emojis.js"></script>
  </head>

  <body>
    <?php include_once 'analytics_tracking.php'; ?>
    <div class="wrapper">
      <form class="form" method="post" action="done.php">
      <h1>Birthday Wishes for Namita</h1>
      <p class="message-part"><br> Add your message below.</p>
      <p class="bg-info user-info">
        <img src="http://graph.facebook.com/<?php echo $user_profile['id'] ?>/picture" />  <span><?php echo $user_profile['name'] ?></span>
      </p>
        ​<textarea rows="5" class="emojis-wysiwyg" placeholder="Message ..."><?php
          $userid = $user_profile['id'];
          $sql = "SELECT quote FROM bidquotes WHERE facebookid='$userid'";
          $result = mysql_query($sql);
          if ($rows = mysql_fetch_array($result)) {
            echo trim($rows['quote']);
          }
        ?></textarea>
        <div id="emoji-but" style="text-align:center;"></div>

        <div style="text-align:center;">
          <input type="submit" onclick="return notext()" class="submit ms" value="Submit">
        </div>
        <div class="value" style="display:none">
          <div class="value"><pre id="emojis-wysiwyg-value"></pre></div>
          <textarea class="dummy-text"  name="quote"></textarea>
        </div>
      </form>

      </div>
      <script type="text/javascript">
      function notext(){
        var quote = $('.emoji-wysiwyg-editor').text().trim();
        if(quote == '')
        {
          alert('Oops! Please enter some text');
          return false;
        }
        else
        {
          var content = $('#emojis-wysiwyg-value').text().trim();
          $(".dummy-text").text(content);
        }
      }
      var $wysiwyg = $('.emojis-wysiwyg').emojiarea({wysiwyg: true});
      var $wysiwyg_value = $('#emojis-wysiwyg-value');
      
      $(".emoji-button").detach().appendTo('#emoji-but');

      $wysiwyg.on('change', function() {
        $wysiwyg_value.text($(this).val());
      });
      $wysiwyg.trigger('change');
    </script>
  </body>
</html>
